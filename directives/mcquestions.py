# Author IUT Blagnac students

"""
MCQ directive.
"""

#Sphynx imports
from docutils.nodes import Element, SkipDeparture
from docutils.parsers.rst import Directive, DirectiveError
#Imports for our code
from random import shuffle
from base64 import b64encode
import json

class MCQuestionsInfo(Element):
    # utiliser le num de rep et check le child
    javascript = """
    <script type="text/javascript">
    function checkAnswer(button, details)
    {
        //HTML part variables
        var thisAnswer = button.value;
        form = button.parentNode.parentNode.parentNode;
        div = button.parentNode.parentNode;
        //Vars
        var nbDivs = button.parentNode.parentNode.parentNode.getElementsByTagName('div').length;
        var dict = {}; 
        //Decode the encoded_string
        var answers_string = decodeURIComponent(escape(window.atob(details)));
        // console.log(answers_string);
        var arrayDetails = answers_string.split(";;"); //Warning : use same string as "block_separator"
        for (let i = 0; i < arrayDetails.length; i++) {
            let array_temp = arrayDetails[i].split(",,"); //Warning : use same string as "subblocks_separator"
            //Check if there is a justification
            let justify_temp = completeDictionary(array_temp);
            dict[array_temp[0]] = {
                "validity" : array_temp[1],
                "justify" : justify_temp,
            };
        }
        if (dict[thisAnswer]["validity"] == 'True'){
            if (div.id == '0'){
                form.className = "quiz_headshot";
            }else{
                form.className = "quiz_ok";
            }
            //Delete the old justify part if there's one
            var element = button.parentNode.parentNode.parentNode.getElementsByTagName('div')[nbDivs-1];
            if(div.id != '0'){
                for(let i = 0; i < element.getElementsByTagName('p').length; i++)
                {
                    element.removeChild(element.getElementsByTagName('p')[i]);
                }
            }
            //Display the answers details
            for(let i = 0; i < arrayDetails.length; i++)
            {
                let tag = document.createElement("p");
                let text = document.createTextNode(dict[i]["justify"]);
                tag.appendChild(text);
                element.appendChild(tag); 
            }
        }else{
            //Change the class' id to have a different color next
            form.className = "quiz_error";
            div.id = "1";
            //Display the answer details
            var tag = document.createElement("p");
            var text = document.createTextNode(dict[thisAnswer]["justify"]);
            tag.appendChild(text);
            var element = button.parentNode.parentNode.parentNode.getElementsByTagName('div')[nbDivs-1];
            //Delete the old justify part if there's one
            if(element.getElementsByTagName('p')[0] != null){
            var nbp = element.getElementsByTagName('p').length;
                for(let i = 0; i < nbp; i++)
                {
                    element.removeChild(element.getElementsByTagName('p')[0]);
                }
            }
            element.appendChild(tag);
        }
    }
    function completeDictionary(array)
    {
        let str = "";
        let true_str = "Oui";
        let false_str = "Non";
        if (array[2] != "None") { // There is a justification
            if (array[1] == "True") { // There is a justification AND the answer is correct
              str += true_str + ". " + array[2] + "."
            }else{ // There is a justification BUT the answer is wrong
              str += array[2] + "."
        }
        }else { // There is'nt a justification
            if (array[1] == "True") {// There is'nt a justification AND the answer is correct
              str += true_str + " !"
            }else{ // There is'nt a justification BUT the answer is wrong
              str += false_str + " !"
            }
        }
        return str;
    }
    </script>
    """
    style = """
    <style type="text/css">
    .quiz_todo
{
padding: 10px;
margin: 10px;
border-radius: 10px;
border: 10px solid #8597C9;
}

.quiz_error
{
    padding: 10px;
    margin: 10px;
    border-radius: 10px;
    border: 10px solid #cf6a6a;
}

.quiz_headshot
{
    padding: 10px;
    margin: 10px;
    border-radius: 10px;
    border: 10px solid #83dd99;
}

.quiz_ok
{
    padding: 10px;
    margin: 10px;
    border-radius: 10px;
    border: 10px solid #539165;
}

.answers
{
    display: flex;
    justify-content: space-around;
    // margin-left: 10%;
    // margin-right: 10%;
    // padding-bottom: 15px;
    // padding-top: 15px;
}


.answers p
{
    text-align: center;
    // font-size: 1.2em;
}

.mcq_question
{
    // font-weight: bold;
    // font-size: 1.1em;
}

#justify
{
    display: flex;
    justify-content: space-around;
    margin-left: 10%;
    margin-right: 10%;
    text-align: center;
    // font-size: 1.3em;
}

#justify p
{
    // padding: 10px;
    flex: 1 1 0;
    max-width: 100%;
    overflow: auto;
}
</style>
    """
    childs = []
    written = False

    def __init__(self, **kwargs):
        Element.__init__(self, **kwargs)

    def visit(self, trans):
        if not MCQuestionsInfo.written:
            trans.head.append(MCQuestionsInfo.javascript)
            trans.head.append(MCQuestionsInfo.style)
            MCQuestionsInfo.written = True

        # .. quiz::'s opening
        opening = '<form class="quiz_todo">\n'
        opening += '<p class="mcq_question">'+self['title']+'</p>'
        trans.body.append(opening)
        # Get content and shuffle it
        MCQuestionsInfo.childs = self.schuffle_children(self.children)

        # Create a string of all justifications and encode it
        block_separator = ";;"
        subblocks_separator = ",,"
        answers_string = ""
        for i in range(len(MCQuestionsInfo.childs)):
            if i != len(MCQuestionsInfo.childs)-1:
                answers_string += str(i) + subblocks_separator + str(MCQuestionsInfo.childs[i]['validity']) + \
                                  subblocks_separator + str(MCQuestionsInfo.childs[i]['justify']) + block_separator
            else:
                answers_string += str(i) + subblocks_separator + str(MCQuestionsInfo.childs[i]['validity']) + \
                                  subblocks_separator + str(MCQuestionsInfo.childs[i]['justify'])
        encoded_string = b64encode(answers_string.encode('utf-8')).decode('ascii')

        # The id of the class is the number of try
        answers = '<div class="answers" id="0">'
        num_answ = 0
        # Create radiobuttons
        for j in MCQuestionsInfo.childs:
            answers += '<p><input type="radio" class="clickable" name="answer" value="' + str(num_answ) + \
                       '" onclick="checkAnswer(this,\''+encoded_string+'\');"/>'+j['title']+"</p>"
            num_answ += 1
        answers += "</div>"
        trans.body.append(answers)

        # The section where the justifi(es) appear after a click on a radio button
        justify = '<div id="justify">'
        justify += '</div>'
        trans.body.append(justify)

        # .. quiz:: departure
        closing = "</form>"
        trans.body.append(closing)

        raise SkipDeparture()

    def schuffle_children(self, childrens):
        copy = []
        for i in childrens:
            copy.append(i)
        shuffle(copy)
        return copy

class MCQuestions(Directive):
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    has_content = True

    node_class = MCQuestionsInfo

    def run(self):
        node = MCQuestionsInfo(title=self.title(), content=self.content)
        self.state.nested_parse(self.content, self.content_offset, node)
        return [node]

    def title(self):
        if self.arguments:
            return self.arguments[0]
        else:
            return None

# Gestion des réponses aux questions
class MCAnswersInfo(Element):
    def __init__(self, **kwargs):
        Element.__init__(self, **kwargs)

    def visit(self, trans):
        pass

class MCAnswers(Directive):

    # correct option
    def correct_answer(value):
        return [value, True]

    # correct answer
    def wrong_answer(value):
        return [value, False]

    required_arguments = 0
    optional_arguments = 1
    final_argument_whitespace = True
    option_spec = {
                    'correct': correct_answer,
                    'wrong': wrong_answer
                  }
    has_content = True

    node_class = MCAnswersInfo

    def run(self):
        params = self.answersValidity()
        node =  MCAnswersInfo(title=self.title(), content=self.content, justify = params[0],validity = params[1])
        return [node]

    def title(self):
        if self.arguments:
            return self.arguments[0]
        else:
            return None

    def answersValidity(self):
        if 'correct' in self.options:
            return self.options.get('correct')
        elif 'wrong' in self.options:
            return self.options.get('wrong')
        else:
            raise ValueError('Expecting an option here')