# Author: Pascal Sotin <pascal.sotin@univ-tlse2.fr>

"""
Improved hints.
"""
import base64

from docutils.nodes import Element
from docutils.parsers.rst import Directive

import static

class HintInfo(Element):
    javascript = """
    <script type="text/javascript">
    function ouvrirFermerHint(span)
    {
        var divContenu = span.parentNode.parentNode.getElementsByTagName('td')[1].getElementsByTagName('div')[0];

        if(divContenu.style.display == 'block')
            divContenu.style.display = 'none';
        else
            divContenu.style.display = 'block';
    }
    </script>
    """
    written = False

    def can_wrap(self):
        return self['can_wrap']

    def wrapped(self):
        return self['wrapped']

    def visit(self, trans):
        if not HintInfo.written:
            trans.head.append(HintInfo.javascript)
            HintInfo.written = True

        png = 'icons/hints/' + self['kind'].lower() + '.png'
        png = static.filename(png)
        base64png = base64.b64encode(open(png, 'rb').read()).decode()
        if self.can_wrap():
            show_hide = ' class="clickable" onclick="ouvrirFermerHint(this);"'
        else:
            show_hide = ''
        if self.wrapped():
            initial_display = 'none'
        else:
            initial_display = 'block'
        opening = f'''<div class="admonitionblock"><table>
        <tr><td class="icon">
        <img alt="My Image" src="data:image/png;base64,{base64png}" {show_hide} />
        </td><td class="content">'''
        if self['title']:
            opening += '<h4' + show_hide + '>' + self['title'] + '</h4>'
        opening += '<div style="display: ' + initial_display + ';">'
        trans.body.append(opening)

    def depart(self, trans):
        closing = '</div></tr></table></div>'
        trans.body.append(closing)


class Hint(Directive):

    def valid_wrap(value):
        accepted_values = ['no', 'maybe', 'yes']
        if value is None or value in accepted_values:
            return value
        else:
            raise ValueError('Expecting a value in ' + str(accepted_values))

    required_arguments = 0
    optional_arguments = 1
    final_argument_whitespace = True
    option_spec = {'wrap': valid_wrap}
    has_content = True

    node_class = HintInfo

    def run(self):
        self.assert_has_content()
        node = HintInfo(kind=self.name, title=self.title(),
                        can_wrap=self.can_wrap(), wrapped=self.wrapped_by_default())
        self.state.nested_parse(self.content, self.content_offset, node)
        return [node]

    def title(self):
        if self.arguments:
            return self.arguments[0]
        else:
            return None

    def wrap(self):
        if 'wrap' in self.options:
            return self.options.get('wrap')
        else:
            return None

    def can_wrap(self):
        if self.title() is None or self.wrap() == 'no':
            return False
        else:
            return self.name in ['note', 'tip', 'lesson', 'help'] or self.wrap() in ['maybe', 'yes']

    def wrapped_by_default(self):
        if not self.can_wrap():
            return False
        elif self.wrap() == 'maybe':
            return False
        else:
            return self.name == 'help' or self.wrap() == 'yes'
