# Author: Pascal Sotin <pascal.sotin@univ-tlse2.fr>

"""
Solution directive.
"""

from docutils.nodes import Element
from docutils.parsers.rst import Directive


class SolutionInfo(Element):

    def __init__(self, **kwargs):
        Element.__init__(self, **kwargs)

    def visit(self, trans):
        opening = "<div class=\"section solution\">\n"
        trans.body.append(opening)

    def depart(self, trans):
        closing = "</div>"
        trans.body.append(closing)


class TeacherInfo(Element):

    def __init__(self, **kwargs):
        Element.__init__(self, **kwargs)

    def visit(self, trans):
        opening = "<div class=\"section teacher\">\n"
        trans.body.append(opening)

    def depart(self, trans):
        closing = "</div>"
        trans.body.append(closing)


class Solution(Directive):
    active = False
    required_arguments = 0
    optional_arguments = 0
    final_argument_whitespace = False
    has_content = True

    node_class = SolutionInfo

    def run(self):
        self.assert_has_content()
        if Solution.active:
            node = SolutionInfo()
            self.state.nested_parse(self.content, self.content_offset, node)
            return [node]
        else:
            return []


class Teacher(Directive):
    active = False
    required_arguments = 0
    optional_arguments = 0
    final_argument_whitespace = False
    has_content = True

    node_class = TeacherInfo

    def run(self):
        self.assert_has_content()
        if Teacher.active:
            node = TeacherInfo()
            self.state.nested_parse(self.content, self.content_offset, node)
            return [node]
        else:
            return []
