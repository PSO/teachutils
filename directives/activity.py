# Author: Pascal Sotin <pascal.sotin@univ-tlse2.fr>

"""
Activity directives (Connect, Observe, Produce).
"""

from docutils.nodes import Element, Inline
from docutils.parsers.rst import Directive


class ActivityInfo(Inline, Element):

    def __init__(self, rawsource='', **kwargs):
        Element.__init__(self, rawsource, **kwargs)

    def visit(self, trans):
        img = '<img src="rst/icons/' + self['label'] + '.png" \\>'
        overlay = '<a href="#" class="info" onclick="return false;">' + img + '<span>' + self['text'] + '</span></a>'
        trans.body.append(overlay)


class Activity(Directive):
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    has_content = False

    node_class = ActivityInfo

    def run(self):
        assert not self.has_content
        text = self.arguments[0]
        node = ActivityInfo(label=self.name, text=text)
        return [node]
