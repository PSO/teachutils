Documentation de rstt2html
==========================

.. contents:: Sommaire

Annotation des sujets
---------------------

Les annotations guident les étudiants dans le sujet
et le structurent visuellement. Certaines sont plus importantes que d'autres.

Insertion d'une annotation
**************************

Pour insérer une annotation **sans titre** :

.. code:: rst

   .. note::

      On notera que la Mer *noire* est en fait *bleue*.

.. note::

   On notera que la Mer *noire* est en fait *bleue*.

Pour insérer une annotation **avec un titre** :

.. code:: rst

   .. note:: Le clavier QWERTY

      Il est remarquable et déplorable que l'agencement
      du clavier QWERTY ait été conçu pour *espacer* les lettres fréquentes.

.. note:: Le clavier QWERTY

   Il est remarquable et déplorable que l'agencement
   du clavier QWERTY ait été conçu pour *espacer* les lettres fréquentes.


Liste et signification des annotations
**************************************

.. note::

   Une :code:`note` apporte des éléments culturels.
   On peut tout à fait ne pas les lire.

.. tip::

   Un :code:`tip` apporte des éléments techniques ou méthodologiques.
   Il s'agit probablement de rappels.
   Souvent placé après un travail à réaliser.
   À lire avant d'appeler le prof.

.. lesson::

   Une :code:`lesson` apporte des éléments théoriques ou techniques.
   Il s'agit probablement de choses nouvelles qui mériteraient
   d'être lues, retenues et qui seraient difficile à deviner.
   Souvent placé avant un travail qui en dépend.

.. important::

   Un :code:`important` annonce des éléments à prendre en considération
   dans ce qui suit.
   Doit éviter aux étudiants de se fourvoyer.

.. caution::

   Un :code:`caution` demande aux étudiants de faire une pause avant de
   continuer.
   Signale un point que les étudiants risquent de prendre à la légère.

.. help::

   Un :code:`help` contient des informations coup de pouce.
   À priori les étudiants devraient ne pas en avoir besoin.
   Mais si l'étudiant.e est bloqué.e, ça permet de se relancer
   même en l'absence du prof.

.. context::

   Un :code:`context` apporte des informations sur le pourquoi
   on fait ça, sur le où on en est.

.. goal::

   Un :code:`goal` présente un objectif à atteindre.
   Cette objectif doit motiver la réalisation de ce qui suit.

Règles de masquage
******************

Une annotation peut être masquable ou non. Si elle est masquable, elle peut-être masquée par défaut ou non.

Pour être masquable, une annotation doit :

- Avoir un titre,
- Ne pas être marquée :code:`:wrap: no`
- Et soit :

  - Être une :code:`note`, :code:`tip`, :code:`lesson`, :code:`help`
  - Être marquée :code:`:wrap: maybe` ou :code:`:wrap: yes`

Pour être masquée par défaut, une annotation doit :

- Être masquable,
- Et soit :

  - Être une :code:`help`,
  - Être marquée :code:`:wrap: yes`

Par exemple :

.. code:: rst

   .. important:: Pourquoi il ne faut pas habiller les animaux
      :wrap: maybe

      - Parce que le cochon salirait ses vêtements
      - Parce que la chèvre les mangerait

.. important:: Pourquoi il ne faut pas habiller les animaux
   :wrap: maybe

   - Parce que le cochon salirait ses vêtements
   - Parce que la chèvre les mangerait


Quiz (en développement)
-----------------------

.. note::

   - En français, *quiz* s'écrit avec un seul *z* au singulier comme au pluriel.
   - En anglais, c'est *quiz* au singulier et *quizzes* au pluriel.

Proposer des quiz dans un sujet permet de le rythmer et de s'assurer qu'une notion est connue, ou apprise sur le tas, avant de poursuivre.

Code couleur :

+----------------+-----------------+-----------------------+
| Bleu-gris      | Rouge           | Vert (foncé / clair ) |
+----------------+-----------------+-----------------------+
| À réaliser     | Réponse erronée | Réponse correcte      |
|                |                 | (du 1e coup / corrigé)|
+----------------+-----------------+-----------------------+

À terme, les quiz seront de trois formes :

- Choix multiple et réponse unique (boutons radio)
- Réponse libre (champ texte)
- Choix multiple et réponses multiples *(checkboxes)*

Question à choix multiple et réponse unique
*******************************************

Exemple :

.. code:: rst

   .. quiz:: Quelle est la capitale du Kenya ?

      .. answer:: Nairobi
         :correct:

      .. answer:: Addis-Abeba
         :wrong: C'est la capitale de l'Éthiopie

      .. anwser:: Mogadiscio
         :wrong: C'est la capitale de la Somalie

.. quiz:: Quelle est la capitale du Kenya ?

  .. answer:: Nairobi
     :correct:

  .. answer:: Addis-Abeba
     :wrong: C'est la capitale de l'Éthiopie

  .. answer:: Mogadiscio
     :wrong: C'est la capitale de la Somalie

.. quiz:: Quelle est la capitale de la France ?

  .. answer:: Paris
     :correct:

  .. answer:: Toulouse
     :wrong: Faut pas rêver.

  .. answer:: Moscou.
     :wrong: Un peu de patience.


Le tag :code:`:correct:` indique la réponse correcte.
Le tag :code:`:wrong` indique une réponse incorrecte.
Ces deux tags sont éventuellement suivis d'un feedback.
On attend au moins deux réponses, dont une et une seule correcte.

Les réponses seront affichées dans un ordre aléatoire ; toujours le même pour un même jeu de réponses.

Affichage du feedback
+++++++++++++++++++++

- En cas de réponse erronée, le feedback *de cette réponse* est affichée, précédé de « Non. » ; en l'absence de feedback, l'affichage est « Non ! ».
- En cas de réponse correcte, *tous* les feedbacks sont affichés, rattachés chacun à leur réponse. Le feedback de la réponse correcte sera précédé de « Oui. » ; en l'absence de ce feedback, l'affichage sera « Oui ! ».

Vérification du texte
+++++++++++++++++++++

Le tag :code:`:pattern:` précise un motif pour vérifier la réponse.

- Si ce motif commence par :code:`/`, on utilise le pattern tel quel en JavaScript.
- Si ce motif ne commence par par :code:`/`, on utilise le pattern :code:`/<motif>/i`
- En l'absence du tag pattern on utilise le pattern :code:`/<réponse_déspécialisée>/i`

Question à réponse libre
************************

Exemple :

.. code:: rst

   .. quiz:: Donner le nom de la méthode qui permet de selectionner un fragment de chaîne de caractère.

      .. answer:: substring

Si le quiz a une unique réponse, ce quiz est réputé à réponse libre et cette réponse est réputée correcte.

Autre exemple :

.. code:: rst

   .. quiz:: Quelle entreprise a lancé le langage Java ?
      :open:

      .. answer:: Sun
         :correct:
         :pattern: Sun (Microsystems?)?

      .. answer:: Oracle
         :wrong: Oracle n'a fait que racheter Java, mais à qui ?

Le tag :code:`:open:` signale une question à réponse libre.

Affichage du feedback
+++++++++++++++++++++

Le feedback d'une question à réponse libre est rattaché au champs texte.

- Si la réponse saisie correspond à la bonne réponse, on affiche « Oui. » suivi du feedback spécifique ou simplement « Oui ! ».
- Si la réponse saisie correspond à une mauvaise réponse, on affiche « Non. » suivi du feedback spécifique ou bien « Non. Ce n'est pas XXX ».
- Si la réponse saisie ne correspond à aucune réponse, on affiche « Non... ».

Question à choix multiple et réponses multiples
***********************************************

Exemple :

.. code:: rst

   .. quiz:: Sélectionnez les fruits.

      .. answer:: Tomate
         :correct: Hé oui. La tomate est un fruit.

      .. answer:: Coing
         :correct:

      .. answer:: Radis
         :wrong:

      .. answer:: Gland
         :correct:

Si un quiz a plusieurs bonnes réponses, il est réputé à réponse multiples.

Les réponses seront affichées dans un ordre aléatoire ; toujours le même pour un même jeu de réponses.

Autre exemple :

.. code:: rst

   .. quiz:: Selectionnez les enseignants du module Algo avancée
      :multiple:

      .. answer:: AP
         :correct: Pour André Péninou.

      .. anwser:: POS
         :wrong:

      .. answer:: PGR
         :wrong:

Sécurité
********

Ni l'examen rapide du code source, ni l'inspection de la page Web ne doivent permettre de déterminer quelles sont les bonnes et les mauvaises réponses.