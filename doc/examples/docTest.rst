Documentation de rstt2html
==========================

.. contents:: Sommaire

Annotation des sujets
---------------------

Les annotations guident les etudiants dans le sujet
et le structurent visuellement. Certaines sont plus importantes que d'autres.

Comment fonctionne un fichier RST ?
***********************************

C'est tres simple:
    - Pour faire un titre, il faut mettre des ** en dessous du dit titre;
    - Ce qu'on veut ecrire n'est qu'a ecrire comme dans un document classique;

.. important:: Mais attention il y a quelque chose de mieux !
    :wrap: no

    On peut ajouter des parties custom


Insertion d'une balise quizz
****************************

.. important:: Ce passages est-il important ?
   :wrap: yes

    En fait non

Un commentaire

.. quiz:: Titre quiz ?

    .. answer:: La reponse a
        :correct: C'est la bonne reponse

    .. answer:: La reponse b
        :wrong: b n'est pas la bonne reponse

    .. answer:: La reponse c
        :wrong: c n'est pas la bonne reponse

    .. answer:: La reponse d
        :wrong:

Une fin de texte

.. quiz:: Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ? Les Chaussettes de l'archiduchesse sont-elles seches, archiseches ?

    .. answer:: Oui
        :correct: Et bien si

    .. answer:: Non
        :wrong: Lel

