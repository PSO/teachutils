#!/usr/bin/python3

"""
A rst2html-like command with extensions dedicated to produce teaching supports.

Inspired by rst2html.py (2006-05-21).
"""

import sys
import locale

from translator import Writer
from docutils.core import publish_cmdline, default_description

locale.setlocale(locale.LC_ALL, '')


description = ('Generates HTML documents from reStructuredText extended to produce teaching supports.'
               + default_description)


def extract_boolean_option(name):
    try:
        sys.argv.remove(name)
        return True
    except ValueError:
        return False


solution = extract_boolean_option('--solution')
teacher = extract_boolean_option('--teacher')

writer = Writer(teacher=teacher,
                solution=solution or teacher)

publish_cmdline(writer=writer, description=description)
