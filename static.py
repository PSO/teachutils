
import os
import sys
from pathlib import Path

root_dir = sys.path[0]
if not root_dir:
    root_dir = os.getcwd()
static_dir = Path(root_dir) / 'static'


def filename(local_name):
    return static_dir / local_name
