
from docutils.writers import html4css1
from docutils.parsers.rst import directives

from directives.activity import Activity
from directives.solution import Solution, Teacher
from directives.hint import Hint
from directives.mcquestions import MCQuestions, MCAnswers

class MyHTMLTranslator(html4css1.HTMLTranslator):

    def visit_SpoilerInfo(self, node):
        node.visit(self)

    def depart_SpoilerInfo(self, node):
        node.depart(self)

    def visit_ActivityInfo(self, node):
        node.visit(self)

    def depart_ActivityInfo(self, node):
        pass

    def visit_TeacherInfo(self, node):
        node.visit(self)

    def depart_TeacherInfo(self, node):
        node.depart(self)

    def visit_SolutionInfo(self, node):
        node.visit(self)

    def depart_SolutionInfo(self, node):
        node.depart(self)

    def visit_HintInfo(self, node):
        node.visit(self)

    def depart_HintInfo(self, node):
        node.depart(self)

    def visit_MCQuestionsInfo(self, node):
        node.visit(self)

    def depart_MCQuestionsInfo(self, node):
        pass

    def visit_MCAnswersInfo(self, node):
        node.visit(self)

    def depart_MCAnswersInfo(self, node):
        pass

    def visit_admonition(self, node):
        kind = node[0][0]
        png = 'rst/icons/hints/' + kind.lower() + '.png'
        del node[0]
        self.body.append('<div class="admonitionblock"><table><tr><td class="icon"><img src="'
                         + png + '"</td><td class="content">')
        html4css1.HTMLTranslator.visit_admonition(self, node)

    def depart_admonition(self, node):
        html4css1.HTMLTranslator.depart_admonition(self, node)
        self.body.append('</tr></table></div>')


class Writer(html4css1.Writer):
    def __init__(self, teacher=False, solution=False):
        Teacher.active = teacher
        Solution.active = solution

        directives.register_directive("dico", Activity)
        directives.register_directive("connect", Activity)
        directives.register_directive("observe", Activity)
        directives.register_directive("produce", Activity)

        directives.register_directive("teacher", Teacher)
        directives.register_directive("solution", Solution)

        directives.register_directive("caution", Hint)
        directives.register_directive("context", Hint)
        directives.register_directive("help", Hint)
        directives.register_directive("lesson", Hint)
        directives.register_directive("important", Hint)
        directives.register_directive("note", Hint)
        directives.register_directive("tip", Hint)
        directives.register_directive("goal", Hint)

        directives.register_directive("quiz", MCQuestions)
        directives.register_directive("answer", MCAnswers)

        html4css1.Writer.__init__(self)
        self.translator_class = MyHTMLTranslator
