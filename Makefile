M := Makefile
DOCDIR := doc
RSTT := python3 rstt2html.py

.PHONY: all
all: doc

.PHONY: doc
doc:
	$(RSTT) $(DOCDIR)/doc-fr.rst --stylesheet doc/examples/style.css --embed-style > $(DOCDIR)/doc-fr.html

# $(RSTT) $(DOCDIR)/doc-fr.rst --stylesheet doc/examples/style.css --embed-style > $(DOCDIR)/doc-fr.html
